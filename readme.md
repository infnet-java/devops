# Trabalho de DevOps da Pós-Graduação MIT em Engenharia de Software com Java

## Projeto

Consiste em uma api com um CRUD básico de tarefas implementado com Spring Boot

## Ambiente

O Projeto está hospedado em uma instância EC2 na AWS. Essa instância foi criada utilizando o Terraform cujo script pode ser encontrado em:

https://gitlab.com/infnet-java/terraform-aws

A versão do java utilizada no ambiente é a 17

O banco de dados utilizado é o H2

## Acesso ao projeto

### Inserir Tarefa

http://3.128.123.245:8080/tasks (POST)

JSON

``
{
"title": "Tarefa 2",
"description": "Teste de cadastro de tarefa 2"
}
``

### Listar Tarefas

http://3.128.123.245:8080/tasks (GET)

#### Detalhar Tarefa

http://3.128.123.245:8080/tasks/1 (GET)

#### Alterar Tarefa

http://3.128.123.245:8080/tasks/1 (PUT)

JSON

``
{
"description": "alterando tarefa"
}
``

### Finalizar Tarefa

http://3.128.123.245:8080/tasks/done/1 (GET)

### Deletar Tarefa

http://3.128.123.245:8080/tasks/1

## Actuator

Para ver os endpoints liberados do actuator basta clicar na url:

http://3.128.123.245:8080/actuator

## Prometheus

http://3.128.123.245:8080/actuator/prometheus

## Deploy

Para efetuar o deploy do projeto no EC2 da AWS, foi utilizada a pipeline do Gitlab

[.gitlab-ci.yml](.gitlab-ci.yml)

[Print do Build](print-build-gitlab.png)

[Print do Deploy pro EC2 na AWS](print-deploy-gitlab-to-ec2.png)
