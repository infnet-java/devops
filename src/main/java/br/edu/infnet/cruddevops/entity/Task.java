package br.edu.infnet.cruddevops.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(columnDefinition = "boolean default false")
    //@Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Boolean done;
    //@Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private LocalDateTime doneAt;
    //@Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private LocalDateTime createdAt;
    //@Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private LocalDateTime updatedAt;
}