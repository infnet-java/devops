package br.edu.infnet.cruddevops.controller;

import br.edu.infnet.cruddevops.entity.Task;
import br.edu.infnet.cruddevops.service.TaskService;
/*import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/tasks")
public class TaskController {

    private static Logger log = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskService taskService;

    /*@Operation(summary = "Cadastrar uma tarefa para um usuário")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Tarefa cadastrada",
                content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Task.class))
                }
            ),
            @ApiResponse(responseCode = "404", description = "Usuário não encontrado!",
                    content = @Content
            )
    })*/
    @PostMapping
    public ResponseEntity<Object> insert(@RequestBody Task task) {

        log.info("Inserindo uma tarefa com os dados {}", task);

        try {

            return ResponseEntity.status(HttpStatus.CREATED).body(taskService.save(task));

        } catch (Exception e) {
            log.info("Erro ao inserir tarefa");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Ocorreu um erro ao tentar inserir a tarefa!");
        }
    }

    /*@Operation(summary = "Listar todas as tarefas")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tarefas",
                    content = {
                            @Content(
                                mediaType = "application/json",
                                array = @ArraySchema(schema = @Schema(implementation = Task.class)))
                    }
            )
    })*/
    @GetMapping
    public ResponseEntity<Object> getAll() {

        log.info("Listar tarefas");

        try{
            return ResponseEntity.status(HttpStatus.OK).body(taskService.findAll());
        } catch (Exception e){
            log.info("Erro ao listar tarefas");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Ocorreu um erro ao tentar listar as tarefas!");
        }
    }

    /*@Operation(summary = "Detalhar uma tarefa")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tarefa",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Task.class))
                    }
            )
    })*/
    @GetMapping("/{id}")
    public ResponseEntity<Object> getOne(@PathVariable(value = "id") Long id) {

        log.info("Detalhar tarefa como id {}", id);

        try {

            Optional<Task> taskOptional = taskService.findById(id);

            if(taskOptional.isEmpty()){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tarefa não encontrada!");
            }

            return ResponseEntity.status(HttpStatus.OK).body(taskOptional.get());

        } catch (Exception e) {
            log.info("Erro ao detalhar a tarefa");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Ocorreu um erro ao tentar buscar a tarefa!");
        }
    }

    /*@Operation(summary = "Alterar uma tarefa")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tarefa alterada",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Task.class))
                    }
            ),
            @ApiResponse(responseCode = "404", description = "Tarefa não encontrada!",
                    content = @Content
            )
    })*/
    @PutMapping("/{id}")
    public ResponseEntity<Object> update(@PathVariable(value = "id") Long id, @RequestBody Task task) {

        log.info("Atualizar tarefa com id {} com os dados {}", id, task);

        try {

            Optional<Task> taskOptional = taskService.findById(id);

            if(taskOptional.isEmpty()){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tarefa não encontrada!");
            }

            Task currentTask = taskOptional.get();
            if (task.getTitle() != null) currentTask.setTitle(task.getTitle());
            if (task.getDescription() != null) currentTask.setDescription(task.getDescription());

            return ResponseEntity.status(HttpStatus.OK).body(taskService.save(currentTask));

        } catch (Exception e) {
            log.info("Erro ao atualizar a tarefa");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Ocorreu um erro ao tentar atualizar a tarefa!");
        }
    }

    /*@Operation(summary = "Deletar uma tarefa")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tarefa deletada!",
                    content = @Content
            ),
            @ApiResponse(responseCode = "404", description = "Tarefa não encontrada!",
                    content = @Content
            )
    })*/
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable(value = "id") Long id) {

        log.info("Deletar tarefa com o id {}", id);

        try {

            Optional<Task> taskOptional = taskService.findById(id);

            if(taskOptional.isEmpty()){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tarefa não encontrada!");
            }

            taskService.delete(taskOptional.get());

            return ResponseEntity.status(HttpStatus.OK).body("Tarefa deletada!");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Ocorreu um erro ao tentar deletar a tarefa!");
        }
    }

    /*@Operation(summary = "Concluir uma tarefa")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tarefa concluída!",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = Task.class))
                    }
            ),
            @ApiResponse(responseCode = "404", description = "Tarefa não encontrada!",
                    content = @Content
            )
    })*/
    @GetMapping("/done/{id}")
    public ResponseEntity<Object> done(@PathVariable(value = "id") Long id) {

        log.info("Finalizar tarefa com o id {}", id);

        try {

            Optional<Task> taskOptional = taskService.findById(id);

            if(taskOptional.isEmpty()){
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Tarefa não encontrada!");
            }

            return ResponseEntity.status(HttpStatus.OK).body(taskService.done(taskOptional.get()));

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Ocorreu um erro ao tentar concluir a tarefa!");
        }
    }
}
