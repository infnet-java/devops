package br.edu.infnet.cruddevops.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

    @GetMapping("/")
    public String home() {
        return "Trabalho - DevOps - Engenharia de Software Java";
    }

}
