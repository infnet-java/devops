package br.edu.infnet.cruddevops.repository;

import br.edu.infnet.cruddevops.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {

}
