package br.edu.infnet.cruddevops.repository;

import br.edu.infnet.cruddevops.entity.Task;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TaskRepositoryTests {

    @Autowired
    private TaskRepository taskRepository;

    // JUnit test for saveTask
    @Test
    @Order(1)
    @Rollback(value = false)
    public void saveTaskTest(){

        Task task = Task.builder()
                .title("Tarefa 1")
                .description("Testar JUnit")
                .done(Boolean.FALSE)
                .build();

        taskRepository.save(task);

        Assertions.assertThat(task.getId()).isGreaterThan(0);
    }

    @Test
    @Order(2)
    public void getTaskTest(){

        Task task = taskRepository.findById(1L).get();

        Assertions.assertThat(task.getId()).isEqualTo(1L);

    }

    @Test
    @Order(3)
    public void getListOfTasksTest(){

        List<Task> tasks = taskRepository.findAll();

        Assertions.assertThat(tasks.size()).isGreaterThan(0);

    }

    @Test
    @Order(4)
    @Rollback(value = false)
    public void updateTaskTest(){

        Task task = taskRepository.findById(1L).get();

        task.setDone(Boolean.TRUE);

        Task taskUpdated =  taskRepository.save(task);

        Assertions.assertThat(taskUpdated.getDone()).isEqualTo(Boolean.TRUE);

    }

    @Test
    @Order(5)
    @Rollback(value = false)
    public void deleteTaskTest(){

        Task task = taskRepository.findById(1L).get();

        taskRepository.delete(task);

        //taskRepository.deleteById(1L);

        Task task1 = null;

        Optional<Task> optionalTask = taskRepository.findById(1L);

        if(optionalTask.isPresent()){
            task1 = optionalTask.get();
        }

        Assertions.assertThat(task1).isNull();
    }

}
